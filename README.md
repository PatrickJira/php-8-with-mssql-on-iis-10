# First try on the PHP 8 with MSSQL and IIS 10 in Windows Server 2019
I try to run the CodeIgniter Framework 4.1.1 there

## Find the right php.ini
First thing first, really
If you can do `phpinfo()` that easy
another way is checking where the php-cgi.exe has been called from iis settings, and check it from there

## Why the sqlsrv extension loaded from MS not showing in phpinfo?
- Check the ini, already add the extension?
- Try running php-cgi.exe in cmd, this may show the error of extension file missing, you'll know where it try to look
- Don't forgot to restart IIS with "iisreset" in cmd with Admin's rights
```
> iisreset
```

## Why PHP cannot write file?
For sure, this thing is about file/folder permission
Check what authority the PHP use first, 
```
<?php
echo exec('whoami');
?>
```
And then using file explorer, right click that folder(s) and select properties. Go to Security tabs, see if the name you got from previous step already there or not.
If already there just simple edit the permission and add read, write or whatever you want.
If it's not there, click on advance button then
1. Click Change Permission (if it's there)
2. Click Add
3. Click Select Principal
4. Put that name in the big box
5. Click Check Names
Then follow the step and add whatever permission you want there, and apply. Done!

## MSSQL don't accept VARCHAR to VARBINARY(max) without converting first
TLDR; I try to use BLOB in ci_sessions in MySQL here. I see many one compare BLOB with VARBINARY(max), that's why the dumb me think of it as a total replacement in MSSQL. As I don't use mssql often. MySQL or MariaDB just accept whatever data to BLOB

The fix is just use VARVHAR(max) instead

## CodeIgniter Remove index.php from URL? How? No official solution
This still not work for me





